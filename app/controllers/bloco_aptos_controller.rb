class BlocoAptosController < ApplicationController
  before_action :set_bloco_apto, only: [:show, :edit, :update, :destroy]

  # GET /bloco_aptos
  # GET /bloco_aptos.json
  def index
    @bloco_aptos = BlocoApto.all.order(:bloco_apto_int)
  end

  # GET /bloco_aptos/1
  # GET /bloco_aptos/1.json
  def show
  end

  # GET /bloco_aptos/new
  def new
    @bloco_apto = BlocoApto.new
  end

  # GET /bloco_aptos/1/edit
  def edit
  end

  # POST /bloco_aptos
  # POST /bloco_aptos.json
  def create
    @bloco_apto = BlocoApto.new(bloco_apto_params)

    respond_to do |format|
      if @bloco_apto.save
        format.html { redirect_to @bloco_apto, notice: 'Bloco apto was successfully created.' }
        format.json { render :show, status: :created, location: @bloco_apto }
      else
        format.html { render :new }
        format.json { render json: @bloco_apto.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bloco_aptos/1
  # PATCH/PUT /bloco_aptos/1.json
  def update
    respond_to do |format|
      if @bloco_apto.update(bloco_apto_params)
        format.html { redirect_to @bloco_apto, notice: 'Bloco apto was successfully updated.' }
        format.json { render :show, status: :ok, location: @bloco_apto }
      else
        format.html { render :edit }
        format.json { render json: @bloco_apto.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bloco_aptos/1
  # DELETE /bloco_aptos/1.json
  def destroy
    @bloco_apto.destroy
    respond_to do |format|
      format.html { redirect_to bloco_aptos_url, notice: 'Bloco apto was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bloco_apto
      @bloco_apto = BlocoApto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bloco_apto_params
      params.require(:bloco_apto).permit(:bloco, :apto, :morador_id)
    end
end
