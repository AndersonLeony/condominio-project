module ApplicationHelper
    TIPOS_MORADORES = ["Proprietário", "Inquilino"]
    # BLOCOS = BlocoApto.pluck(:bloco).uniq
    # APTOS = BlocoApto.pluck(:apto).uniq

    def options_for_tipos_func_application_helper(selected)
        options_for_select(TIPOS_MORADORES, selected)
    end

    def options_for_blocos_func_application_helper(selected)
        options_for_select(get_blocos(), selected)
    end

    def options_for_aptos_func_application_helper(selected)
        options_for_select(get_aptos(), selected)
    end

    private
    def get_blocos()
        array = []
        12.times do |i|
            array << "#{i+1}"
        end
        array
    end

    def get_aptos()
        array = []
        apto = 1
        16.times do |i|
            if i.between?(0,3)
                array<<"00#{apto}"
                apto = apto + 1
            elsif i.between?(4,7)
                if (i+1) == 5
                    apto = 101
                    array<<"#{apto}"
                    apto = apto + 1
                else
                    array<<"#{apto}"
                    apto = apto + 1
                end
            elsif i.between?(8,11)
                if (i+1) == 9
                    apto = 201
                    array<<"#{apto}"
                    apto = apto + 1
                else
                    array<<"#{apto}"
                    apto = apto + 1
                end
            elsif i.between?(12,15)
                if (i+1) == 13
                    apto = 301
                    array<<"#{apto}"
                    apto = apto + 1
                else
                    array<<"#{apto}"
                    apto = apto + 1
                end
            else
            end 
        end
        array
    end

end
