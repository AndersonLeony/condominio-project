class Morador < ApplicationRecord
    has_many :carros, dependent: :destroy
    has_many :telefones, dependent: :destroy
    has_one :bloco_apto, dependent: :destroy

    accepts_nested_attributes_for :bloco_apto
    accepts_nested_attributes_for :carros, reject_if: :all_blank, allow_destroy: true
    accepts_nested_attributes_for :telefones, reject_if: :all_blank, allow_destroy: true
end
