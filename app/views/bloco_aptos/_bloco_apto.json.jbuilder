json.extract! bloco_apto, :id, :bloco, :apto, :morador_id, :created_at, :updated_at
json.url bloco_apto_url(bloco_apto, format: :json)
