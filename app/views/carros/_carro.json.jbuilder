json.extract! carro, :id, :placa, :modelo, :cor, :morador_id, :created_at, :updated_at
json.url carro_url(carro, format: :json)
