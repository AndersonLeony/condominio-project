json.extract! morador, :id, :nome, :cpf, :email, :tipo, :created_at, :updated_at
json.url morador_url(morador, format: :json)
