json.extract! telefone, :id, :num_telefone, :morador_id, :created_at, :updated_at
json.url telefone_url(telefone, format: :json)
