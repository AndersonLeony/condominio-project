Rails.application.routes.draw do
  #get 'home/index'
  root 'home#index'
  resources :bloco_aptos
  resources :telefones
  resources :carros
  resources :moradors
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
