class CreateMoradors < ActiveRecord::Migration[5.2]
  def change
    create_table :moradors do |t|
      t.string :nome
      t.string :cpf
      t.string :email
      t.string :tipo
      t.integer :bloco_apto_int
    end
  end
end
