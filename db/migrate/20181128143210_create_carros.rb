class CreateCarros < ActiveRecord::Migration[5.2]
  def change
    create_table :carros do |t|
      t.string :placa
      t.string :modelo
      t.string :cor
      t.references :morador, foreign_key: true
    end
  end
end
