class CreateTelefones < ActiveRecord::Migration[5.2]
  def change
    create_table :telefones do |t|
      t.string :num_telefone
      t.references :morador, foreign_key: true
    end
  end
end
