class CreateBlocoAptos < ActiveRecord::Migration[5.2]
  def change
    create_table :bloco_aptos do |t|
      t.string :bloco
      t.string :apto
      t.integer :bloco_apto_int
      t.references :morador, foreign_key: true
    end
  end
end
