namespace :utils do
  desc "TODO"
  task seed: :environment do
    tipo_morador = ["Proprietário", "Inquilino"]
    
    puts "Gerando os Moradores (Moradors)..."
    
    192.times do |i|
        Morador.create!(
            nome: Faker::Name.name,
            cpf: Faker::CPF.pretty,
            email: Faker::Internet.email,
            tipo: "Proprietário"
        )
    end
    
    puts "Gerando os Moradores (Moradors)...[OK]\n\n"
    
    puts "Gerando os Blocos e Aptos para cada Morador..."
    bloco = 1
    bloco_str = "1"
    apto_str = "001"
    apto = 1
    cont_andares = 1
    qtde_aptos = 1

    Morador.all.each do |morador|
      morador.bloco_apto_int = "#{bloco_str}#{apto_str}".to_i
      morador.save!

      if(qtde_aptos == 1)
        apto_str = "001"
      end

      BlocoApto.create!(
        bloco:"#{bloco_str}",
        apto:"#{apto_str}",
        bloco_apto_int: "#{bloco_str}#{apto_str}".to_i,
        morador_id: morador.id
      )
      
      qtde_aptos = qtde_aptos + 1
        
      if qtde_aptos.between?(1, 4)
        if(qtde_aptos == 1)
          apto = 1
        else
          apto = apto + 1
        end
        apto_str = "00#{apto}"

      elsif qtde_aptos.between?(5, 8)
        if(qtde_aptos == 5)
          apto = 101
        else
          apto = apto + 1
        end
        apto_str = "#{apto}" 
      elsif qtde_aptos.between?(9, 12)
        if(qtde_aptos == 9)
          apto = 200 + 1
        else
          apto = apto + 1
        end    
        apto_str = "#{apto}"
      elsif qtde_aptos.between?(13, 16)
        if(qtde_aptos == 13)
          apto = 300 + 1
        else
          apto = apto + 1
        end  
        apto_str = "#{apto}"
      else
        bloco = bloco + 1
        bloco_str = bloco.to_s
        qtde_aptos = 1
        apto = 1
        apto_str = "001"
      end

    end

    puts "Gerando os Tipos para cada Morador...[OK]\n\n"

  end

end
