require 'test_helper'

class BlocoAptosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bloco_apto = bloco_aptos(:one)
  end

  test "should get index" do
    get bloco_aptos_url
    assert_response :success
  end

  test "should get new" do
    get new_bloco_apto_url
    assert_response :success
  end

  test "should create bloco_apto" do
    assert_difference('BlocoApto.count') do
      post bloco_aptos_url, params: { bloco_apto: { apto: @bloco_apto.apto, bloco: @bloco_apto.bloco, morador_id: @bloco_apto.morador_id } }
    end

    assert_redirected_to bloco_apto_url(BlocoApto.last)
  end

  test "should show bloco_apto" do
    get bloco_apto_url(@bloco_apto)
    assert_response :success
  end

  test "should get edit" do
    get edit_bloco_apto_url(@bloco_apto)
    assert_response :success
  end

  test "should update bloco_apto" do
    patch bloco_apto_url(@bloco_apto), params: { bloco_apto: { apto: @bloco_apto.apto, bloco: @bloco_apto.bloco, morador_id: @bloco_apto.morador_id } }
    assert_redirected_to bloco_apto_url(@bloco_apto)
  end

  test "should destroy bloco_apto" do
    assert_difference('BlocoApto.count', -1) do
      delete bloco_apto_url(@bloco_apto)
    end

    assert_redirected_to bloco_aptos_url
  end
end
