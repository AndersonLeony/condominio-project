require "application_system_test_case"

class BlocoAptosTest < ApplicationSystemTestCase
  setup do
    @bloco_apto = bloco_aptos(:one)
  end

  test "visiting the index" do
    visit bloco_aptos_url
    assert_selector "h1", text: "Bloco Aptos"
  end

  test "creating a Bloco apto" do
    visit bloco_aptos_url
    click_on "New Bloco Apto"

    fill_in "Apto", with: @bloco_apto.apto
    fill_in "Bloco", with: @bloco_apto.bloco
    fill_in "Morador", with: @bloco_apto.morador_id
    click_on "Create Bloco apto"

    assert_text "Bloco apto was successfully created"
    click_on "Back"
  end

  test "updating a Bloco apto" do
    visit bloco_aptos_url
    click_on "Edit", match: :first

    fill_in "Apto", with: @bloco_apto.apto
    fill_in "Bloco", with: @bloco_apto.bloco
    fill_in "Morador", with: @bloco_apto.morador_id
    click_on "Update Bloco apto"

    assert_text "Bloco apto was successfully updated"
    click_on "Back"
  end

  test "destroying a Bloco apto" do
    visit bloco_aptos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bloco apto was successfully destroyed"
  end
end
